# DownloadFile
import requests
from django_cyversedev.django_cyversedev import settings


def downloadFile(dest):
    req_url = 'https://de.cyverse.org/terrain/secured/fileio/download?path=' + dest
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})
    print(r.json())


downloadFile('/iplant/home/srishtitiwari/BigWig_HomoSapien.bigWig')
