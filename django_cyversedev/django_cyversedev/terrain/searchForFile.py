import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def searchForFile(exact, label, size):
    #request_data = '{"query": {"all":[{"type": "label","args": {"exact": true, "label": "ML_Project"}},{"type": "owner","args": {"owner": "pawanbole"}}]},"size": 10,"from": 0,"sort": [{"field": "dateCreated","order": "descending"}]}'

    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/search'
    username = settings.username

    request_Data = RequestCreator.create_searchFile_request(exact, label,  username, size)
    r = requests.post(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken,
                                        'Connection': 'keep-alive',
                                        'Content-Type': 'application/json'},
                                        data= request_Data)

    json_data = r.json()
    response = ResponseParser.parse_searchResponse(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res
