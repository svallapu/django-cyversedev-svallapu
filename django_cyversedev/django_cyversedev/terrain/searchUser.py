import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def searchForUser(label):
    # request data =  'https://de.cyverse.org/terrain/subjects?search=srishti'
    req_url = 'https://de.cyverse.org/terrain/subjects?search='+label
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})

    json_data = r.json()
    response = ResponseParser.parse_subjects(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res
