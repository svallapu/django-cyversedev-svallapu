import urllib3


def getSpeciesVersionList():
    http = urllib3.PoolManager()
    r = http.request('GET', 'http://www.igbquickload.org/quickload/contents.txt')
    fileaslist = r.data.splitlines()
    trimmedList =[]
    finalList = []
    for x in fileaslist:
        trimmedList.append(x.decode().replace('\t', ' '))

    for x in trimmedList:
        finalList.append(" ".join(x.split(' ', 3)[:3]))

    keyValList = []
    for x in finalList:
        str1 = x.split(' ', 1)[1]
        str2 = x.split(' ', 1)[0]
        keyValList.append(str1+","+str2)

    speciesversionlist = {}
    for i in keyValList:
        j = i.split(',')
        if j[0] in speciesversionlist:
            speciesversionlist[j[0]].append(j[1])
        else:
            speciesversionlist[j[0]]=[j[1]]
    return speciesversionlist


print(getSpeciesVersionList())