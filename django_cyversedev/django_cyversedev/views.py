import json
from . import settings
from django.shortcuts import render
from django.http import HttpResponse
from .terrain.getUserData import getUserData
from .terrain.getSharedData import getSharedData
from .terrain.getCommunityData import getCommunityData
from .terrain.getUserDetail import getUserDetails
from .terrain.searchForFile import searchForFile
from .terrain.searchUser import searchForUser
from .terrain.getfileid import getfileid
from .terrain.getMetadata import getMetadata
from .terrain.saveMetadata import saveMetadata
from .terrain.shareFile import shareFile
from .terrain.unshareFile import unshareFile
from .terrain.checkFilePermissions import checkFilePermissions
from .terrain.getBasicAuth import getBasicAuth


import sys


def index(request):
    context = {}
    return render(request, 'index.html', context)


def home(request):
    context = {}
    return render(request, 'django_cyversedev/middlepanel.html', context)


def loginCyverse(request):
    userData = getUserData('', '20', '0', 'NAME', 'ASC')
    context = {
        "userData": userData
    }
    return render(request, 'base.html', context)


def base(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            response = getUserData(path, limit, offset, sortcol, sortdir)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def oauth(request):
    context = {}
    return render(request, 'oauth.html', context)

def getFileIdApi(request):
    response = None

    if request.is_ajax():
        try:
            path = request.GET.get('url')
            if path is None:
                raise Exception("The request header does not contain Path")

            response = getMetadata(path)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        #raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


def retrieveMetaData(request):
    response = None

    if request.is_ajax():
        try:
            fileId = request.GET.get('fileId')
            if fileId is None:
                raise Exception("The request header does not contain fileId")

            response = getMetadata(fileId)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        #raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)

def saveMetaData(request):
    # fileId = ""
    # data = [{"attr":"Genome","unit":"integratedGenomeBrowser","value":"somegenome"},{"attr":"foreground","unit":"integratedGenomeBrowser","value":"1211212"},{"attr":"background","unit":"integratedGenomeBrowser","value":"1211212"}]

    response = None
    if request.is_ajax():
        try:
            fileId = request.POST['fileId']
            data_attrib = json.loads(request.POST['data'])

            if fileId is None:
                raise Exception("The request header does not contain fileId")
            response = saveMetadata(fileId, data_attrib)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        #raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


def renderSharedData(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            response = getSharedData(path, limit, offset, sortcol, sortdir)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

def getCommunityDataList(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            response = getCommunityData(path, limit, offset, sortcol, sortdir)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

def setPublicLink(request):
    response = None
    if request.is_ajax():
        try:
            path = request.GET.get('url')
            response = shareFile(path, 'anonymous')
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

def getRootPathUser(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            accesstoken = settings.accesstoken
            response = getUserDetails(accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def searchFile(request):
    response = None
    if request.is_ajax():
        try:
            exact = request.GET.get('match')
            label = request.GET.get('label')
            size = request.GET.get('size')

            response = searchForFile.searchForFile(exact, label, size)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

def remPublicLink(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            response = unshareFile(path, "anonymous")

        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

def checkFilePermission(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            response = checkFilePermissions(path)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        context = {}
        return render(request, 'middlepanel.html', context)

def searchtheUser(request):
    response = None
    if request.is_ajax():
        try:
            label = request.GET.get('label')
            response = searchForUser(label)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


