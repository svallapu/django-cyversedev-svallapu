function rightPanelUI(data){

    $(".right").data("fileInfo",data)
    $(".right").find('.pathLink').html("FilePath: "+data.path)
}
function rightPanel(section) {
    // var parseData=$.parseJSON(data)

    if(section=="#makePublic"){
         rightPanelInitial($(".right").data("fileInfo").path)
    }

    $(".middle").removeClass("col-10")
    $(".middle").addClass("col-7")
    $(".right").css("display", "block")
    rpStatus = 1
    $(".right").children().css("display", "none");
    $(section).css("display", "block");
    loadingDisplay(".rightLoad",true)
}
function rightPanelInitial(path){

    urlLink=location.origin+"/checkFilePermission?url="+path
    $.ajax({
            method: "GET",
            dataType: "html",
        	async: "true",
			url: urlLink,
	        success: function (data, status, jqXHR) {
                var userPerm=JSON.parse(data).paths[0]["user-permissions"]
                for(var i=0;i<=userPerm.length;i++){
                    if(userPerm.length!=0&&userPerm[i].user=="anonymous" && userPerm[i].permission=="read"){
                            manageLinkUI("create",path)
                            break;
                    }else{
                            manageLinkUI("remove",path)
                    }
                }
                loadingDisplay(".rightLoad",false)

            },
            error: function (jqXHR, status, err) {
                alert(err)
			},
            complete: function (jqXHR, status) {
            }
	    });
}
function manageLinkUI(ele,path) {
    if (ele == "remove") {
        $('#remove').css("display", "none")
        $('#create').css("display", "block")
        $('#plink').val("")
        $('#copy').css("visibility", 'hidden')
        $('.fileStatus').removeClass("btn-warning")
        $('.fileStatus').addClass("btn-info")
        $('.fileStatus').html("Private")

    } else {
        $('#remove').css("display", "block")
        $('#create').css("display", "none")
        $('#plink').val(path)
        $('#copy').css("visibility", 'visible')
        $('.fileStatus').addClass("btn-warning")
        $('.fileStatus').removeClass("btn-info")
        $('.fileStatus').html("Public")
    }
}

function manageLinkOperations(ele){
   loadingDisplay(".rightLoad",true)
   var urlLink=""
   if (ele == "remove") {
       urlLink=location.origin+"/remPublicLink?url="+$(".right").data("fileInfo").path
   }else{
       urlLink=location.origin+"/setPublicLink?url="+$(".right").data("fileInfo").path

   }
     $.ajax({
            method: "GET",
            dataType: "html",
        	async: "true",
			url: urlLink,
	        success: function (data, status, jqXHR) {
                if(ele=="create"){
	                if(JSON.parse(data).sharing[0].user=="anonymous"&&JSON.parse(data).sharing[0].paths[0].permission=="read"){
	                    manageLinkUI(ele,JSON.parse(data).sharing[0].paths[0].path)
                    }
                }else{
                    if(JSON.parse(data).unshare[0].user=="anonymous"&& JSON.parse(data).unshare[0].unshare[0].success==true){
	                    manageLinkUI(ele,JSON.parse(data).unshare[0].unshare[0].path)
                    }
                }
                loadingDisplay(".rightLoad",false)
            },
            error: function (jqXHR, status, err) {
                alert(err)
			},
            complete: function (jqXHR, status) {
            }
	    });
}
