var rpStatus = 0;
tableData = [];
var recordsLimit = 100;

$(document).ready(function (e) {
    rpStatus = 0;
    getFileList('', recordsLimit, 0, "NAME", "ASC", "home", true)
});
$(document).click(function(){
    $("#context-menu").hide();
});

var homePath = "/iplant/home/nowlanf/"
var homePathforShare = '/iplant/home/'
var homePathforCommunity = '/iplant/home/shared/'
function setupPagination(data, enable_prev, enable_next, defaultaction){

        var pages = 0
        var total = parseInt(data["total"])
        var pages_rem = total%recordsLimit
        if (pages_rem == 0)
        {
            pages = parseInt((total/recordsLimit))
        }
        else {
            pages = parseInt((total/recordsLimit)) + 1
        }
        if (pages == 0){
            $('#pagetext').hide();
            $('#page-selection').hide();
            return
        }
        else{
            $('#pagetext').show();
            $('#page-selection').show();
        }


        // logic to enable disable the next and previous
        if (defaultaction) {
            if (pages == 1) {
                enable_prev = false;
                enable_next = false;
            } else if (pages > 1) {
                 enable_next = '»';
                 enable_prev = false;
            }
        }
        else{
            var currentpage = parseInt($("#page-selection").data("currentPage"));
            if (currentpage == 1){
                enable_prev = false;
                enable_next = '»';
                $('#page-selection').bootpag({next: enable_next, prev: enable_prev})
            }
            else if(currentpage == pages){
                   enable_next = false;
                   enable_prev = '«';
                   $('#page-selection').bootpag({next: enable_next, prev: enable_prev})

            }
            else{
                enable_next = '»';
                enable_prev = '«';
                $('#page-selection').bootpag({next: enable_next, prev: enable_prev})
            }
        }

        // continue from here , to seperate the code.


        if (defaultaction){
        // init bootpag
        $('#page-selection').bootpag({
            total: pages,
            maxVisible: 5,
            next: enable_next,
            prev: enable_prev
        }).on("page", function(event, /* page number here */ num){

             $("#page-selection").data("currentPage", num)
             var section = $("#page-selection").data("currentSection")
             if (section == "home"){
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, 'NAME', 'ASC','home', false) // some ajax content loading...
             }
             else if (section == "shared"){
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, 'NAME', 'ASC','shared', false) // some ajax content loading...
             }
             else if(section == "community"){
                 getFileList($("#page-selection").data("currentPath") , recordsLimit, (num-1)*recordsLimit, 'NAME', 'ASC','community', false) // some ajax content loading...
             }
        });
        }
}

function getFileList(url, limit, offset, sortcol, sortdir, section, defaultaction) {
//	var sessionid = Cookies.get('session');
//	if (sessionid==null) {loading(true);
        closerp()
        var baseurl=""
        var func=""
        loadingDisplay(".middleLoad",true)
        if(section=="community"){
            baseurl="/getCommunityData?url="
            func="getCommunityData"
        }else if(section=="shared"){
            baseurl="/renderSharedData?url="
            func="getShareFileList"
        }else{
            baseurl="/base?url="
            func="getFileList"
        }
	    $.ajax({
            method: "GET",
            dataType: "html",
        	async: "true",
			url: location.origin+baseurl+url+"&limit="+recordsLimit+"&offset="+offset+"&sortcol="+sortcol+"&sortdir="+sortdir,
	        success: function (data, status, jqXHR) {
                $("#page-selection").data("currentPath",url)
                $("#page-selection").data("currentSection", section)
                setupPagination(JSON.parse(data), false, false, defaultaction)
	            interpretData(JSON.parse(data), func)
	            buildBreadCrumbs(JSON.parse(data), func)
	            loadingDisplay(".middleLoad",false)
            },
            error: function (jqXHR, status, err) {
                alert(err)
			},
            complete: function (jqXHR, status) {
            }
	    });
//    }
}

function interpretData(data, functiontype){
    $('#fileFolder').children('tbody').html("")
    populateTable(data["folders"], "folders")
    populateTable(data["files"], "files")
    $("tr").click(function () {
        $("tr").removeClass('active');
        $(this).addClass('active');
        if ($(this).hasClass("file").toString() == "true") {
            $(".right").data("fileInfo", $(this).data("info"))
        }
    });
    $('tr').on({contextmenu:function(e) {
        if ($(this).hasClass("file").toString() == "true") {
                e.preventDefault();
                $('#context-menu').css({'display': 'block', 'left': e.pageX, 'top': e.pageY});
                rightPanelUI($(this).data("info"))
            }
        },
        click: function(e) {
            var $target = $(e.target);
            if ($target.is('#context-menu') || $target.closest('#context-menu').length) {
                e.preventDefault();
            } else {
                $("#context-menu").hide();
            }
        }
    });
    $("tr").dblclick(function () {
        var fldrpth=""
        rightPanelUI($(this).data("info"))
        if($(this).hasClass("folder").toString()=="true") {
            if (functiontype == 'getFileList') {
                fldrpth ="/"+$(this).data("info").path.split(homePath)[1];
                getFileList(fldrpth, recordsLimit, 0, "NAME", "ASC","home", true)
            } else if (functiontype == 'getShareFileList') {
                fldrpth = "/"+$(this).data("info").path;
                getFileList(fldrpth, recordsLimit, 0, "NAME", "ASC","shared", true)
            } else if (functiontype == 'getCommunityData') {
                fldrpth = "/"+$(this).data("info").path
                getFileList(fldrpth, recordsLimit, 0, "NAME", "ASC","community", true)
            }
        }

    });
}

function populateTable(tableData, type) {
    var menuButton = '<button class="btn btn-success mr-1">View in IGB</button>\n' +
        '<button type="button" class="btn" data-toggle="dropdown" onclick="alert()"><img src="/static/img/3dot.png" height="25"  />'

    if (type == "folders") {
        for (var data in tableData) {
            var row = "<tr id='" + tableData[data].id + "' class='folder'><td><i class='fas fa-folder mr-1'></i>" + tableData[data].label + "</td><td>" + tableData[data].dateModified + "</td><td>-</td><td></td></tr>"
            $('#fileFolder').children('tbody').append(row)
            $("#" + tableData[data].id).data("info", tableData[data])
        }
    } else if (type = "files") {
        for (var data in tableData) {
            var row = "<tr  id='" + tableData[data].id + "' class='file'><td title=" + tableData[data].label + "><i class='far fa-file mr-1'></i>" + tableData[data].label + "</td><td>" + tableData[data].dateModified + "</td><td>" + tableData[data].fileSize + "</td><td>" + menuButton + "</td></tr>"
            $('#fileFolder').children('tbody').append(row)
            $("#" + tableData[data].id).data("info", tableData[data])
        }
    }
}

function buildBreadCrumbs(data,functiontype) {
    $(".breadcrumb").html('')
    $(".breadcrumb").data("currentPath",data.fileSystemInfo.path)
    var link = ""
    if (functiontype == 'getFileList') {
        var directories = data.fileSystemInfo.path.split(homePath)[1]
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(\'\',recordsLimit,0,\'NAME\',\'ASC\',\'home\', true)"><i class="fas fa-home mr-1"></i></a></li>')
        if(directories!=null){
           link=""
           for (var dir in directories.split('/')) {
                   link = link+"/"+ directories.split('/')[dir]
                   $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(\'' + link + '\',recordsLimit,0,\'NAME\',\'ASC\',\'home\', true)">' + directories.split('/')[dir] + '</a></li>')
           }
       }
    } else if (functiontype == 'getShareFileList') {
        var directories = data.fileSystemInfo.path.split(homePathforShare)[1]
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(homePathforShare,\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'shared\', true)"><i class="fas fa-user-plus mr-1"></i></a></li>')
        if(directories!=null){
            link=""
            for (var dir in directories.split('/')) {
                link = link+"/"+ directories.split('/')[dir]
                $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(\'' + homePathforShare+link + '\',\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'shared\', true)">' + directories.split('/')[dir]+ '</a></li>')
            }
        }
    } else if (functiontype == 'getCommunityData') {
        var directories = data.fileSystemInfo.path.split(homePathforCommunity)[1]
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(homePathforCommunity,\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'community\', true)"><i class="far fa-clock mr-1"></i></a></li>')
        if(directories!=null){
            link=""
            for (var dir in directories.split('/')) {
                link = link+"/"+ directories.split('/')[dir]
                $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(\'' +homePathforCommunity+link + '\',\'recordsLimit\',\'0\',\'NAME\',\'ASC\',\'community\', true)">' + directories.split('/')[dir]+ '</a></li>')
            }
        }
    }

}

function closerp() {
    $(".middle").removeClass("col-8")
    $(".middle").addClass("col-10")
    $(".right").css("display", "none")
    rpStatus = 0;
}
function loadingDisplay(loading,bool){
    if(bool){
        $(loading).css("display","block")
    }else{
        $(loading).css("display","none")
    }
}
function complete() {
    alert("Congratulations! The Task is Complete.")
}




